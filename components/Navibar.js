import { Fragment, useState, useEffect, useContext } from 'react';
import { Navbar, Nav, Button } from 'react-bootstrap';
import Link from 'next/link';
import UserContext from '../contexts/UserContext'
import { useRouter } from 'next/router';
import { RiLogoutCircleLine } from "react-icons/ri";
import { AiOutlineLogin } from "react-icons/ai";

import { RiUserAddLine } from "react-icons/ri";




export default () => {
	const {user} = useContext(UserContext);
	const [ isExpanded, setIsExpanded ] = useState(false);

	//lets declare 2 variables that will describe 2 sections of our navbar
	let RightNavOptions
	let LeftNavOptions
	const router = useRouter();
	//lets create a control struction which will determine the options displayed in the navbar.

	if(user.email !== null){

		RightNavOptions = (
			<Fragment>
				<Link href="/logout">
					<a className="nav-link"> {user.email} | <RiLogoutCircleLine/>Logout </a>
				</Link>
			</Fragment>
			)
		LeftNavOptions = (
			<Fragment>
				<Link href="/user/categories">
					<a className="nav-link"> Categories </a>
				</Link>
				<Link href="/user/records">
					<a className="nav-link"> Records </a>
				</Link>
				<Link href="/user/charts/category-breakdown">
					<a className="nav-link"> Breakdown </a>
				</Link>
			</Fragment>
			)
	}else{
		if(router.pathname === '/login'){
			RightNavOptions =(
				<Fragment>
					<Link href="/register">
						<a className="nav-link"><RiUserAddLine />  Register </a>
					</Link>
				</Fragment>
			) 				
			LeftNavOptions = null;
		}else if(router.pathname === '/'){
			RightNavOptions =(
				<Fragment>
					<Link href="/register">
						<a className="nav-link"><RiUserAddLine />  Register </a>
					</Link>
					<Link href="/login">
					<a className="nav-link"> <AiOutlineLogin /> Login </a>
				</Link>
				</Fragment>
			) 				
			LeftNavOptions = null;

		}else {
			RightNavOptions = (					
				<Fragment>
				<Link href="/login">
					<a className="nav-link"> <AiOutlineLogin /> Login </a>
				</Link>
				</Fragment>
			)
		}

	}
		
	return (
		<Fragment>
			<Navbar expanded={ isExpanded } expand="lg" fixed="top"className="navbar" variant="dark">
				 <Navbar.Brand href="/" className="brand-font">
				      <img
				        alt=""
				        src="/tw.png"
				        width="30"
				        height="30"
				        className="d-inline-block align-top"
				      	/>{'ThriftyWallet'}
				    </Navbar.Brand>
					   	<Navbar.Toggle onClick={ () => setIsExpanded(!isExpanded) } aria-controls="basic-navbar-nav" />
				   	 <Navbar.Collapse className="basic-navbar-nav">
				        <Nav className="mr-auto" onClick={ () => setIsExpanded(!isExpanded) }>
				        	{ LeftNavOptions  }
				        </Nav>
				        <Nav className="ml-auto" onClick={ () => setIsExpanded(!isExpanded) }>
				        	{ RightNavOptions }
				        </Nav>

				</Navbar.Collapse>
			</Navbar>
		</Fragment>
		)
}