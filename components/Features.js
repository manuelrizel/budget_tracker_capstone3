
import { Container } from 'react-bootstrap'
import { FaCalculator,FaWallet } from "react-icons/fa";
import { FiPieChart } from "react-icons/fi";



export default function Features(){
	return (

	<div className="feature-section" id="features">
		<h2 class="h1-responsive font-weight-bold my-5">What are the benefits?</h2>
  			<p class="lead grey-text w-responsive mx-auto mb-5">Find out various features that you can access for free.</p>
  	<div class="row">
	    <div class="col-md-4 feature-box">
	    	<FaCalculator size={90} className="feature-icon" />
	      <h5 class="font-weight-bold my-4">Budget Planning</h5>
	      <p class="grey-text mb-md-0 mb-5">Plan your budget and stay ahead of your goal.</p>
	    </div>

	    <div class="col-md-4 feature-box">
	    <FiPieChart size={90} className="feature-icon" />
	      <h5 class="font-weight-bold my-4">Overview of Records</h5>
	      <p class="grey-text mb-md-0 mb-5">Records are projected so you can see it at a glance.
	      </p>
	    </div>

	    <div class="col-md-4 feature-box">
    	<FaWallet size={90} className="feature-icon" />
	      <h5 class="font-weight-bold my-4">Track your spending</h5>
	      <p class="grey-text mb-0"> Know where you put your money into and monitor how you spend it.
	      </p>
	    </div>
  	</div>
  </div>



	)
}