import Head from 'next/head'
import styles from '../styles/Home.module.css'
import View from '../components/View'; 
import Banner from '../components/Banner'
import Features from '../components/Features'



export default function Home() {
	

  return (
  	<div>
      <View title={ 'ThriftyWallet' }>
      <Banner />

      </View>
       <Features />
     </div>
  )
}
