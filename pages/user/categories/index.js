import { useState, useEffect } from 'react'
import { Table, Button } from 'react-bootstrap'
import Link from 'next/link'
import View from '../../../components/View'
import AppHelper from '../../../app-helper';
import { FaPlusCircle } from "react-icons/fa";
import { MdFormatListBulleted } from "react-icons/md";



export default () => {
   
    const [categories, setCategories] = useState([])
    useEffect(() => {
            const payload = {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
                }, 
                body: JSON.stringify({
                    name: name
                })
            }

        fetch(`${AppHelper.API_URL}/users/get-categories`, payload).then((response) => response.json()).then(categories => {

            let showCategory
                showCategory = categories.map((category) => {
                    const textColor = (category.type === 'Income') ? 'text-success' : 'text-danger'
                    return (
                        <tr key={category._id}>
                            <td>{category.name}</td>
                            <td className={textColor}>{category.type}</td>
                        </tr>
                        )
                })
                setCategories(showCategory)

        }) 
},[])

    return (
        <View title="Categories">
        <div className="col-md-12">
            <div className="row">
                <div className="col-md-6 cat-title">
                    <h3><MdFormatListBulleted /> Categories </h3> 
                </div>
                <div className="col-md-6 cat-button"> 
                <Link href="/user/categories/new"><a className="btn btn-success mt-1 mb-3"><FaPlusCircle size={15} className="icon-style" /> Add New Category </a></Link>
                </div>
            </div>
            <div className="row">
            <Table className="styled-table" bordered>
          <thead>
              <tr>
                    <th>Category</th>
                    <th>Type</th>      
              </tr>
              </thead>
                <tbody>
                    {categories}
                </tbody>             
            </Table>
            </div>
        </div>
            
            
            
        </View>
    )
}
