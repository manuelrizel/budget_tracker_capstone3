import { useState, useEffect } from 'react';
import { InputGroup, Form, Col } from 'react-bootstrap';
import { Pie, Doughnut } from 'react-chartjs-2';
import View from '../../../components/View';
import moment from 'moment';
import randomcolor from 'randomcolor';
import AppHelper from '../../../app-helper';
import { FiPieChart } from "react-icons/fi";

export default () => {
    const [fromDate, setFromDate] = useState(moment().subtract(1, 'months').format('YYYY-MM-DD'))
    const [toDate, setToDate] = useState(moment().format('YYYY-MM-DD'))
    const [labelsArr, setLabelsArr] = useState([])
    const [dataArr, setDataArr] = useState([])
    const [bgColors,setBgColors] = useState([])
    const [totalArr, setTotalArr] = useState([0])



    const data = {
            labels: labelsArr,
            datasets: [
                {
                    label: 'Category',
                    data: dataArr,
                    backgroundColor: bgColors
                }
            ],
            hoverOffset: 4
        };
    const dataType = {
            labels: ['Income', 'Expense'],
            datasets: [
                {
                    data: totalArr,
                    backgroundColor: ['#A1F6A1','#F6A1A1' ]
                }
            ],
            hoverOffset: 4
        };

     
     useEffect(() => {
        const payload = {
           method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
            }, 
            body: JSON.stringify({
                fromDate: fromDate,
                toDate: toDate
            })
        }

        fetch(`${AppHelper.API_URL}/users/get-records-breakdown-by-range`, payload).then((response) => response.json()).then(records => {
                setLabelsArr(records.map(record => record.categoryName))    
                setDataArr(records.map(record => record.totalAmount))
                setBgColors(records.map(()=> randomcolor()))

                let typeSumIncome =0
                for(let i=0; i< records.length; i++){
                    if(records[i].type === 'Income'){
                        typeSumIncome += records[i].totalAmount
                    }
                }
  
                let typeSumExpense =0
                for(let i=0; i< records.length; i++){
                    if(records[i].type === 'Expense'){
                        typeSumExpense += records[i].totalAmount
                    }
                }
                setTotalArr([typeSumIncome, typeSumExpense])
                
            })
        
            

    },[fromDate, toDate])

    return (
        <View title="Category Breakdown">
            <h3><FiPieChart/> Category Breakdown</h3>
            <Form.Row>
                <Form.Group as={ Col } xs="6">
                    <Form.Label>From</Form.Label>
                    <Form.Control type="date" value={ fromDate } onChange={ (e) => setFromDate(e.target.value) }/>
                </Form.Group>
                <Form.Group as={ Col } xs="6">
                    <Form.Label>To</Form.Label>
                    <Form.Control type="date" value={ toDate } onChange={ (e) => setToDate(e.target.value) }/>
                </Form.Group>
            </Form.Row>
            <div className="chartbox mb-5">
            <div className="row">
                <div className="col-md-6">
                <Pie data={data} height={220}/>
                </div>
                <div className="col-md-6">
                <Doughnut data={dataType} height={220}/>
                </div>
                </div>
            </div>
        </View>
    )
}
